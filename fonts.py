# Copyright 2020 Portmod Authors
# Distributed under the terms of the GNU General Public License
# Either version 3 of the License, or any later version

import sys
import os
import shutil
import logging
from typing import cast, Dict, List, Mapping, Optional, Tuple, Union
from xml.dom.minidom import Node, getDOMImplementation, parse

from portmodlib.module_util import create_file
from portmodlib.execute import execute
from fontTools.ttLib import TTFont


def get_code_list(font: str):
    fontobj = TTFont(font)
    cmap = fontobj["cmap"]
    return list(cmap.getBestCmap().keys())


def get_config_dir() -> str:
    config_dir: Optional[str] = None
    if sys.platform == "win32":
        paths = os.path.expanduser(os.environ["OPENMW_CONFIG_DIR"]).split(";")
    else:
        paths = os.path.expanduser(os.environ["OPENMW_CONFIG_DIR"]).split(":")

    for path in paths:
        if os.path.exists(path):
            config_dir = path

    if paths and not config_dir:
        config_dir = paths[0]
        logging.warning(
            f"OpenMW config directory could not be found. Using directory {config_dir}"
        )
    elif not config_dir:
        raise Exception(
            "Could not find openmw config directory! "
            "Make sure your OPENMW_CONFIG_DIR setting is correct"
        )
    assert config_dir
    return config_dir


def get_selected_fonts(root_dir: str) -> Dict[str, str]:
    font_dir = os.path.join(get_config_dir(), "Fonts")
    path = os.path.join(font_dir, "openmw_font.xml")

    selected_fonts = {}
    if os.path.exists(path):
        dom = parse(path)
        root = dom.documentElement
        for child in root.childNodes:
            if child.nodeType == Node.ELEMENT_NODE and child.getAttribute("name") in {
                "Magic Cards",
                "Daedric",
                "MonoFont",
            }:
                for node in child.childNodes:
                    if (
                        node.nodeType == Node.ELEMENT_NODE
                        and node.getAttribute("key") == "Source"
                    ):
                        font_path = os.path.join(font_dir, node.getAttribute("value"))
                        if os.path.islink(font_path):
                            font_path = os.readlink(font_path)
                        if not os.path.exists(font_path):
                            logging.warning(
                                f'Font {child.getAttribute("name")} in openmw_font.xml '
                                f"references nonexistant file at path {font_path}"
                            )
                        selected_fonts[child.getAttribute("name")] = font_path
    else:
        # Select default fonts from scratch
        for font_type in ["daedric", "magic", "mono"]:
            options = find_available_fonts(root_dir, font_type)

            def find_font(name: str, font_list: List[Tuple[str, str]]) -> Optional[str]:
                for path, font_name in font_list:
                    if name == font_name:
                        return path
                return None

            full_type = get_full_font_type(font_type)

            if font_type == "daedric" and find_font("OMW Ayembedt", options):
                selected_fonts[full_type] = cast(
                    str, find_font("OMW Ayembedt", options)
                )
            elif font_type == "magic" and find_font("Pelagiad", options):
                selected_fonts[full_type] = cast(str, find_font("Pelagiad", options))
            elif font_type == "mono" and find_font("DejaVu Sans Mono", options):
                selected_fonts[full_type] = cast(
                    str, find_font("DejaVu Sans Mono", options)
                )
            elif options:
                # Otherwise, choose the first font in the list
                selected_fonts[full_type] = options[0][0]
        create_font_xml(selected_fonts)

    return selected_fonts


def check_selected_fonts(root_dir: str, fonts: Dict[str, str]):
    for font in fonts:
        if not os.path.exists(fonts[font]):
            path = get_font_path(font, os.path.basename(fonts[font]), root_dir)
            if path:
                print(f"Fixing invalid font path {fonts[font]} to {path}")
                fonts[font] = path


def create_font_xml(fonts: Mapping[str, str]):
    """
    Create openmw_font.xml using the given font configuration
    """
    font_dir = os.path.join(get_config_dir(), "Fonts")
    path = os.path.join(font_dir, "openmw_font.xml")
    for font in fonts:
        dest_file = create_file(os.path.join(font_dir, os.path.basename(fonts[font])))
        try:
            os.symlink(fonts[font], dest_file)
        except OSError:
            shutil.copy(
                fonts[font],
                dest_file,
            )

    if os.path.exists(path):
        dom = parse(path)
        root = dom.documentElement
    else:
        impl = getDOMImplementation()
        dom = impl.createDocument(None, "MyGUI", None)
        root = dom.documentElement

    root.setAttribute("type", "Resource")
    root.setAttribute("version", "1.1")

    def append_child(root, name, attrs):
        elem = dom.createElement(name)
        for key in attrs:
            elem.setAttribute(key, attrs[key])
        root.appendChild(elem)
        return elem

    for child in root.childNodes:
        if child.nodeType == Node.ELEMENT_NODE and child.getAttribute("name") in fonts:
            root.removeChild(child)
            child.unlink()

    for font in sorted(fonts):
        elem = append_child(
            root, "Resource", {"type": "ResourceTrueTypeFont", "name": font}
        )

        append_child(
            elem, "Property", {"key": "Source", "value": os.path.basename(fonts[font])}
        )
        if font == "MonoFont":
            size = "17"
        else:
            size = "24"
        append_child(elem, "Property", {"key": "Size", "value": size})
        append_child(elem, "Property", {"key": "Resolution", "value": "50"})
        append_child(elem, "Property", {"key": "Antialias", "value": "false"})
        append_child(elem, "Property", {"key": "TabWidth", "value": "8"})
        append_child(elem, "Property", {"key": "OffsetHeight", "value": "0"})
        codes = append_child(elem, "Codes", {})
        code_list = get_code_list(fonts[font])
        start = code_list[0]
        prev = start
        for code in code_list[1:]:
            if code == prev + 1:
                prev = code
                continue

            append_child(codes, "Code", {"range": f"{start} {prev}"})
            start = code
            prev = code

        append_child(codes, "Code", {"range": f"{start} {prev}"})

    xml_string = dom.toprettyxml()
    xml_string = os.linesep.join(  # remove the weird newline issue
        [s for s in xml_string.splitlines() if s.strip()]
    )
    path = create_file(path)
    with open(path, "w") as file:
        file.write(xml_string)


def is_mono(file: str) -> bool:
    """Determines if the font is a monospace font"""
    if shutil.which("fc-scan"):
        spacing = execute('fc-scan -f "%{spacing}" "' + file + '"', pipe_output=True)
        # If a fixed spacing is provided, it's a mono font
        if spacing:
            return True

    name = get_name(file)
    return "mono" in name.lower()


def is_normal_style(file: str) -> bool:
    """Determines if the font is not Bold, Italic or Oblique"""
    if shutil.which("fc-scan"):
        style = execute('fc-scan -f "%{style}" "' + file + '"', pipe_output=True)
        return all(value not in style for value in {"Bold", "Italic", "Oblique"})

    name = get_name(file).lower()
    return all(value not in name for value in {"bold", "italic", "oblique"})


def get_name(file: str) -> str:
    """Determines the name of the font"""
    if shutil.which("fc-scan"):
        name = execute('fc-scan -f "%{fullname}" "' + file + '"', pipe_output=True)
    else:
        name, _ = os.path.splitext(os.path.basename(file))
    return str(name)


def find_available_fonts(root: str, font_type: str) -> List[Tuple[str, str]]:
    found = []
    font_dir = os.path.join(root, "share", "fonts")
    installed_fonts = []
    if os.path.exists(font_dir):
        installed_fonts = os.listdir(font_dir)
    for name in installed_fonts:
        name, ext = os.path.splitext(name)
        if ext.lower() == ".ttf":
            path = os.path.join(root, "share", "fonts", name + ext)
            if font_type == "mono" and is_mono(path) and is_normal_style(path):
                found.append((path, get_name(path)))
            elif font_type != "mono" and not is_mono(path):
                found.append((path, get_name(path)))
    # For daedric and magic, only list fonts which are installed by portmod
    # (maybe allow some sort of configuration to list more?)
    if font_type == "mono":
        # Use fc-list to determine available system mono fonts
        if shutil.which("fc-list"):
            fonts = execute(
                "fc-list :fontformat=TrueType:spacing=mono", pipe_output=True
            ).split("\n")
            for fontline in fonts:
                if not fontline.strip():
                    continue
                fontline_splited = fontline.split(":")
                if len(fontline_splited) == 2:
                    path, name = fontline_splited
                    styles = ""
                else:
                    path, name, styles = fontline_splited
                if "Bold" in styles or "Italic" in styles or "Oblique" in styles:
                    continue
                found.append((path.strip(), name.strip()))

    if font_type == "magic":
        # Use fc-list to determine available system fonts
        if shutil.which("fc-list"):
            fonts = execute("fc-list :fontformat=TrueType", pipe_output=True).split(
                "\n"
            )
            for fontline in fonts:
                if not fontline.strip():
                    continue
                fontline_splited = fontline.split(":")
                if len(fontline_splited) == 2:
                    path, names = fontline_splited
                    styles = ""
                else:
                    path, name, styles = fontline_splited
                if (
                    "Bold" in styles
                    or "Italic" in styles
                    or "Oblique" in styles
                    or is_mono(path)
                ):
                    continue
                found.append((path.strip(), name.strip()))

    return found


def list_fonts(font_type: str, root_dir: str):
    fonts = find_available_fonts(root_dir, font_type)
    if os.path.exists(os.path.join(get_config_dir(), "Fonts", "openmw_font.xml")):
        selected = get_selected_fonts(root_dir)
    else:
        selected = {}
    num = 0
    print(f"Available fonts for {get_full_font_type(font_type)}:")
    maxlength = max(len(name) for _, name in fonts)
    numlen = len(str(len(fonts)))
    for path, name in fonts:
        if path == selected.get(get_full_font_type(font_type)):
            print(
                f"  [{str(num).rjust(numlen, ' ')}] * {name.ljust(maxlength, ' ')} ({path})"
            )
        else:
            print(
                f"  [{str(num).rjust(numlen, ' ')}]   {name.ljust(maxlength, ' ')} ({path})"
            )
        num += 1


def get_font_path(font_type, font: Union[str, int], root: str):
    font_path = None
    fonts = find_available_fonts(root, font_type)
    if isinstance(font, str) and font.isdigit() or isinstance(font, int):
        # Find font by index
        (font_path, _) = fonts[int(font)]
    else:
        # Find font by name and set
        for path, name in fonts:
            noext, _ = os.path.splitext(os.path.basename(path))
            if name == font or os.path.basename(path) == font or noext == font:
                font_path = path
                break
    return font_path


def set_font(root: str, font_type: str, font: Union[str, int]):
    font_path = get_font_path(font_type, font, root)

    full_font_type = get_full_font_type(font_type)

    fonts = get_selected_fonts(root)
    print(f"Setting {full_font_type} to {font_path}")
    fonts[full_font_type] = font_path
    check_selected_fonts(root, fonts)

    create_font_xml(fonts)


def get_full_font_type(font_type: str) -> str:
    if font_type == "daedric":
        return "Daedric"
    if font_type == "magic":
        return "Magic Cards"
    if font_type == "mono":
        return "MonoFont"
    raise Exception("Invalid font type " + font_type)


def get_short_font_type(font_type: str) -> str:
    if font_type == "Daedric":
        return "daedric"
    if font_type == "Magic Cards":
        return "magic"
    if font_type == "MonoFont":
        return "mono"
    raise Exception("Invalid font type " + font_type)
